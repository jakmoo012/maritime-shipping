import { Component, OnInit } from '@angular/core';
import { CONTACT_US_CONTENT } from 'src/app/screens/contact-us/contact-us-content';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public content = CONTACT_US_CONTENT;

  constructor() {}

  ngOnInit() {}
}
