import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit, AfterViewInit {
  private social: HTMLElement;
  private links: HTMLElement;
  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    this.setSocial();
    window.addEventListener('resize', this.setSocial);
  }

  setSocial() {
    this.social = document.getElementById('social');
    if (window.innerWidth > 991) {
      this.social.setAttribute('class', 'order-last');
    } else {
      this.social.setAttribute('class', 'order-first');
    }
  }

  toggleMenu() {
    switch (this.social.getAttribute('class')) {
      case 'order-first':
        setTimeout(() => {
          this.social.setAttribute('class', 'order-last');
        }, 380);
        break;
      case 'order-last':
        this.social.setAttribute('class', 'order-first');
        break;
      default:
        this.social.setAttribute('class', 'order-last');
    }
  }
}
