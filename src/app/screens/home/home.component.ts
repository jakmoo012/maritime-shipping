import { Component, OnInit } from '@angular/core';
import { CONTENT } from './home-content';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public content = CONTENT;
  constructor() {}

  ngOnInit() {}
}
