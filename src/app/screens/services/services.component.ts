import { Component, OnInit } from '@angular/core';
import { CONTENT } from './services-content';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  public content = CONTENT;
  constructor() {}

  ngOnInit() {}
}
