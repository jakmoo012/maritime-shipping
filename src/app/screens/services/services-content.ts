export const CONTENT = {
  title: 'Services',
  sea: [
    `Maritime Shipping and Logistics offers sea freight forwarding services
     at competitive rates by working with an extensive array of international
     carriers.  We also provide a complete range of import and export
     movements by sea.  Working with the world’s best international carriers,
     we guarantee a first class service.`,
    `Whether you require Full Container Loads (FCL) or Less than Container
      Loads (LCL) we have the container for you. We will ensure that all aspects
       of freight, Customs and haulage are handled reliably and efficiently.`,
    `Bulk cargo services are also available. Maritime Shipping and Logistics can
     transport your goods as dry bulk or break bulk loads.`
  ],
  road: [
    `Through our network of haulage partners we offer a full suite of road haulage
     packages throughout the UK and Europe. We offer a container transport service
     from all major UK ports at competitive rates.`,
    `We can supply specialist vehicles dependent on your load, from tail lifts to
      Tautliners or vans we will get your load to its destination.`
  ],
  warehouse: [
    `From our warehousing facilities at the Port of Tyne we can supply warehousing
     with full picking and packing facilities. With our full service from unloading
     your cargo, securing store and reliable forwarding, we can supply you with a
     full logistics service. `
  ]
};
