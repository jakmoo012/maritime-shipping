export const CONTENT = {
  title: 'About us',
  paragraphs: [
    `At Maritime Shipping and Logistics we specialise in international sea freight.
     Be it bulk or containerised goods, we can deliver worldwide. With extensive
     experience in agricultural and biomass products we know how to move bulk cargo.`,
    `From our base at the Port of Tyne, in the north east of England, our dedicated
     team will supply a personal, professional service. We can get your goods to their
     destination whether it be by sea, air or road, on time, every time.`,
    `Our port side warehousing, combined with our network of haulier partners, also
    allows us to quickly and efficiently forward your products throughout the UK. Our
    close proximity to the A1 and A19 allows unrivalled access to the UK road network.`
  ]
};
