import { Component, OnInit } from '@angular/core';
import { CONTENT } from './about-content';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  public content = CONTENT;

  constructor() {}

  ngOnInit() {}
}
