import { Component, OnInit } from '@angular/core';
import { CONTACT_US_CONTENT } from './contact-us-content';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  public content = CONTACT_US_CONTENT;

  constructor() {}

  ngOnInit() {}
}
