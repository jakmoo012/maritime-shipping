export const CONTACT_US_CONTENT = {
  title: 'Contact us',
  phone: '0191 428 7700',
  email: 'maritimeshippingandlogistics66@gmail.com',
  address: {
    lineOne: 'Tynegrain Silos',
    lineTwo: 'Tyne Dock',
    lineThree: 'South Shields',
    postcode: 'NE34 9PL'
  }
};
